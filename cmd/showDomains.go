// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showDomainsCmd represents the showDomains command
var showDomainsCmd = &cobra.Command{
	Use:   "showDomains",
	Short: "Show domain names",
	Long:  `Show domain names for each IP in the database`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("showDomains called")
	},
}

var domainProject string

func init() {
	projectCmd.AddCommand(showDomainsCmd)

	showDomainsCmd.Run = showdomains

	showDomainsCmd.Flags().StringVarP(&domainProject, "project", "p", "", "Project name")
	//showDomainCmd.Flags().StringVarP(&AddDomainIP, "host", "i", "", "Host IP")
	showDomainsCmd.MarkFlagRequired("project")

}

func showdomains(cmd *cobra.Command, args []string) {
	hosts := database.GetHosts(domainProject)
	for _, host := range hosts {
		domains := database.GetDomain(domainProject, host)
		if domains != nil {
			fmt.Printf("Domains for  Host: %s\n", host)
			for domain := range domains {
				fmt.Printf("-  %s\n", domains[domain])
			}
		}
	}

}
