package portscan

import (
	"crypto/tls"
	"log"
	"net/http"
	"strings"
)

func GetHttpServer(Host string, Port string) string {

	// This is to ignore self made TLS certs
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	url := "http://" + Host + ":" + Port

	// Perform HTTP HEAD
	response, err := http.Head(url)
	if err != nil {
		log.Fatal("Error fetching URL. ", err)
	}

	// Print out each header key and value pair
	for key, value := range response.Header {
		//headerLine := key + " " + value[0]
		//fmt.Println(headerLine)
		//fmt.Printf("%s: %s\n", key, value[0])
		if strings.Contains(key, "Server") {
			return value[0]
		}
	}
	return "0"
}
