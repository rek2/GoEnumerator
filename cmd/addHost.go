// Copyright © 2018 Rek2, Fernandez rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"
	"net"

	"github.com/spf13/cobra"
)

// addHostCmd represents the addHost command
var addHostCmd = &cobra.Command{
	Use:   "addHost",
	Short: "Add new IP to a project or operation",
	Long: `Add a new IP to a project or operation, make sure is in numeric form. 
	Example: 192.168.100.100`,
}

var AddHostName, AddProjectName string

func init() {
	projectCmd.AddCommand(addHostCmd)

	addHostCmd.Run = addhost

	addHostCmd.Flags().StringVarP(&AddProjectName, "project", "p", "", "Project name")
	addHostCmd.Flags().StringVarP(&AddHostName, "host", "i", "", "Host IP")
	addHostCmd.MarkFlagRequired("project")
	addHostCmd.MarkFlagRequired("host")
}

func addhost(cmd *cobra.Command, args []string) {
	addr := net.ParseIP(AddHostName)
	if addr == nil {
		fmt.Printf("*** Please enter an IP address! ***\n*** for Host use addDomain ***\n")
	} else {
		database.AddHost(AddProjectName, AddHostName)
	}
	// wee need to enforce adding URL instead of IP, later we can convert to IP with a simple dns query
}
