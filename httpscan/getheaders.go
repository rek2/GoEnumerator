package httpscan

import (
	"crypto/tls"
	"log"
	"net/http"
)

func GetHeaders(Host string, Port string) []string {

	// This is to ignore self made TLS certs
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}
	url := "http://" + Host + ":" + Port
	// Perform HTTP HEAD
	response, err := http.Head(url)
	if err != nil {
		log.Fatal("Error fetching URL. ", err)
	}

	var allHeaders []string

	// Print out each header key and value pair
	for key, value := range response.Header {
		headerLine := key + ":" + value[0]
		//fmt.Printf("%s: %s\n", key, value[0])
		//if strings.Contains(key, "Server") {
		//	targetPorts[port] = value[0]
		//}

		allHeaders = append(allHeaders, headerLine)
	}
	return allHeaders
}
