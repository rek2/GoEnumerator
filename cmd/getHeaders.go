// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/httpscan"
	"fmt"

	"github.com/spf13/cobra"
)

// getHeadersCmd represents the getHeaders command
var getHeadersCmd = &cobra.Command{
	Use:   "getHeaders",
	Short: "Get host domains http headers",
	Long:  `Get the headers for each of the domains for a certain host in the database and save it`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("getHeaders called")
	},
}

var headersProjectName, headersHostName string

func init() {
	webToolsCmd.AddCommand(getHeadersCmd)

	getHeadersCmd.Run = getheaders

	getHeadersCmd.Flags().StringVarP(&headersProjectName, "project", "p", "", "Project name")
	getHeadersCmd.MarkFlagRequired("project")
	getHeadersCmd.Flags().StringVarP(&headersHostName, "host", "i", "", "Host IP")

}

func getheaders(cmd *cobra.Command, args []string) {

	if len(headersHostName) > 0 {

		//here we do it with one host, no need to check for list of hosts.
		fmt.Println("NOT YET IMPLEMENTED, SOON!")
	} else {

		allHosts := database.GetHosts(headersProjectName)
		for host := 0; host < len(allHosts); host++ {
			var allHeaders []string
			allHTTPports := database.GetHTTPPorts(headersProjectName, allHosts[host])

			if len(allHTTPports) > 0 {
				allDomains := database.GetDomain(headersProjectName, allHosts[host])
				for port := 0; port < len(allHTTPports); port++ {
					// if the host has domains do the domains instead of ip, I should change the logic later
					if len(allDomains) > 0 {
						for domain := 0; domain < len(allDomains); domain++ {
							headers := httpscan.GetHeaders(allDomains[domain], allHTTPports[port])
							if len(headers) > 0 {
								for _, header := range headers {
									allHeaders = append(allHeaders, allDomains[domain]+","+allHTTPports[port]+","+header)
								}
							}
						}
					} else {
						headers := httpscan.GetHeaders(allHosts[host], allHTTPports[port])

						if len(headers) > 0 {
							for _, header := range headers {
								allHeaders = append(allHeaders, allHosts[host]+","+allHTTPports[port]+","+header)
							}
						}
					}
				}
				if len(allHeaders) > 0 {
					fmt.Printf("\nHeaders for Host: %s\n", allHosts[host])
					fmt.Println(allHeaders)
					database.AddHeaders(headersProjectName, allHosts[host], allHeaders)
				}
			} else {
				fmt.Printf("\nNo HTTP ports in the database for HOST: %s, Run a portscan first!!!!\n", allHosts[host])
			}
		}

	}
}
