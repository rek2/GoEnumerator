// Copyright © 2018 ReK2, Fernandez rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// portscanCmd represents the portscan command
var portscanCmd = &cobra.Command{
	Use:   "portscan",
	Short: "Diff types of port scanning",
	Long:  `Diferent types of port scan to select. For example: `,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			cmd.Help()
			os.Exit(0)
		}
	},
}

func init() {
	rootCmd.AddCommand(portscanCmd)

	//portscanCmd.Flags().BoolP("TCPScan", "t", false, "Run a TCP scan")
}
