// Copyright © 2018 Rek2, Fernandez rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"

	"github.com/spf13/cobra"
)

// showHostsCmd represents the showHosts command
var showHostsCmd = &cobra.Command{
	Use:   "showHosts",
	Short: "Show all hosts for a project",
	Long: `Show all hosts for a project in the database 
	you must to add project name`,
}

var ShowHostsProjectName string

func init() {
	projectCmd.AddCommand(showHostsCmd)

	showHostsCmd.Run = showhosts

	showHostsCmd.Flags().StringVarP(&ShowHostsProjectName, "project", "p", "", "Project name")
	showHostsCmd.MarkFlagRequired("project")
}

func showhosts(cmd *cobra.Command, args []string) {
	database.ShowHosts(ShowHostsProjectName)

}
