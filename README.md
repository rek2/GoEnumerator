[![pipeline status](https://gitlab.com/rek2/GoEnumerator/badges/master/pipeline.svg)](https://gitlab.com/rek2/GoEnumerator/commits/master)


## ABOUT GoEnumerator

- GoEnumerator is a tool I'm writing for the fun of it but to also use for my own enumeration, so I will be keep adding more features to try to speed up the first steps
on any CTF enumeration or profesional pentest, as I will add features and options to make the tool to not trigger IDS's etc.
- Why GoEnumerator? well as what I say above, and also wanted to have tools that write to a database, so I can later export the data or use Postgresql UI's to organize the data,
or easy export to other tools that hopefully I also will make :) 

## HOWTO
- copy GoEnumerator.yaml or create it on ~/.GoEnumerator.yaml
- Fill it up with your database creds, see "Database configuration below" for hel

## FRONT UI TOOLS
- QGoEnumerator [ Comming SOON!] - Official front end to GoEnumerator with
- https://wiki.postgresql.org/wiki/Community_Guide_to_PostgreSQL_GUI_Tools
- https://www.pgadmin.org/
- [Libre Office](https://www.libreoffice.org/download/download/) - native driver for PostgreSQL databases [HowTo Configure LibreOffice for PostGreSQL](https://superuser.com/questions/391242/how-do-i-connect-to-a-postgresql-server-using-libreoffice-base/463639) you can create reports and tons of nice things with the Database wizard, export to html,create pentest reports with your own templates etc!

## Bash completion
- To have all the commands and subcomands using tab/bash auto completion  etc just run:
```
. <(GoEnumerator  completion)
```

- To configure your bash shell to load completions for each session add to your bashrc:
```
# ~/.bashrc or ~/.profile
echo '. <(GoEnumerator completion)' >> ~/.bashrc
```

## Database configuration ##

- install postgresql
```
  yay -S postgresql 
```
- Initialize it
```
sudo -u postgres -i
initdb --locale en_US.UTF-8 -E UTF8 -D '/var/lib/postgres/data'  #initialize main database cluster
```

- start it
```
  systemctl start postgresql
```
- create creds and database
```
  createuser --interactive --pwprompt    #enter no to all the questions, because you dont want to give privileges to the user thats going to use this application
  createdb GoEnumerator -U <username_we_created_above>
```
- Acess the database shell
```
  psql -d GoEnumerator
  \c #GoEnumerator #connect to the particular database
  \du #list all users and their permission level
  \dt #list sum about all tables in the datatabase
```
- remove your history
```
  rm ~/.psql_history
```


[Click here for more info](https://wiki.archlinux.org/index.php/PostgreSQL)

## Example use:

- __Add Project/Operation__
```
[blackarch files ]$ GoEnumerator project addOp hackthebox
Using config file: /home/rek2/.GoEnumerator.yaml
hackthebox
```
- __Add IP/Host to Project or Operation__
```
[blackarch files ]$ GoEnumerator project addHost -i 10.10.10.105 -p hackthebox
Using config file: /home/rek2/.GoEnumerator.yaml
hackthebox 10.10.10.105
[ blackarch files ]$ GoEnumerator project showHosts -p hackthebox
Using config file: /home/rek2/.GoEnumerator.yaml
--- Operation hackthebox ---
Hosts:
- 10.10.10.105
```
- __Project/Operation list commands__
```
[blackarch ~ ]$ GoEnumerator project -h
Adds, Modify or Remove operations
Usage:
  GoEnumerator project [flags]
  GoEnumerator project [command]
Available Commands:
  addDomain    Add hostname or domain to a project IP/host
  addHost      Add new IP to a project or operation
  addOp        Add new operations/project name to database
  removeDomain Remove a domain from the list of domain names
  removeHost   Remove a HOST IP from a project or operation
  removeOp     Remove your operation or project from the database
  showDomains  Show domain names
  showHosts    Show all hosts for a project
  showOps      List all Ops and Projects
Flags:
  -h, --help   help for project
Global Flags:
      --config string   config file (default is $HOME/.GoEnumerator.yaml)
Use "GoEnumerator project [command] --help" for more information about a command.
```
- __Port Scan options__
```
[ blackarch ~ ]$ GoEnumerator portscan -h
Diferent types of port scan to select. For example:
Usage:
  GoEnumerator portscan [flags]
  GoEnumerator portscan [command]
Available Commands:
  getBanners  gets ports found on database and try's to get the banner of what is running
  showBanners Show saved banners for each host
  showPorts   Show ports
  tcpScan     Simple tcp port scan
Flags:
  -h, --help   help for portscan
Global Flags:
      --config string   config file (default is $HOME/.GoEnumerator.yaml)
Use "GoEnumerator portscan [command] --help" for more information about a command.
```
- __showBanners option__
```
 blackarch ~ ]$ GoEnumerator portscan showBanners -p cia
Using config file: /home/rek2/.GoEnumerator.yaml
Banners for Host: 104.236.237.78
-  80,Apache/2.4.18 (Ubuntu)
-  22,SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.4
-  443,Apache/2.4.18 (Ubuntu)
Banners for Host: 165.227.62.19
-  22,SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.4
-  443,nginx/1.10.3 (Ubuntu)
Banners for Host: 10.8.0.1
-  22,SSH-2.0-OpenSSH_7.2p2 Ubuntu-4ubuntu2.4
```

- __Search for exploits from banners and get ID:__
```
GoEnumerator exploitDB search  -f 10 Nginx
Using config file: /home/rek2/.GoEnumerator.yaml
____________________________________________________
+ ID: 32277
+ Description: Nginx 1.4.0 (Generic Linux x64) - Remote Overflow
+ Type: linux_x86-64
+ https://www.exploit-db.com/exploits/32277
____________________________________________________
+ ID: 25775
+ Description: Nginx 1.3.9 < 1.4.0 - Chuncked Encoding Stack Buffer Overflow (Metasploit)
+ Type: linux
+ https://www.exploit-db.com/exploits/25775
____________________________________________________
+ ID: 40768
+ Description: Nginx (Debian Based Distros + Gentoo) - 'logrotate' Local Privilege Escalation
+ Type: linux
+ https://www.exploit-db.com/exploits/40768
____________________________________________________
+ ID: 9901
+ Description: Nginx 0.7.0 < 0.7.61 / 0.6.0 < 0.6.38 / 0.5.0 < 0.5.37 / 0.4.0 < 0.4.14 - Denial of Service (PoC)
+ Type: linux
+ https://www.exploit-db.com/exploits/9901
Showing entries 11-20 of 14. Completed in 299.778µs
```

## Compile from src
- have a Go env setup $GOPATH etc.
- build for Gnu+linux:
```
make
```
- build for OSX+darwing:
```
make darwin
```
- build for Windows:
```
make windows
```

if you are already in any of this OS's just run make

- To build and install run:
```
make install
```
and make sure your $GOPATH/bin is in your shell $PATH
```
export $PATH:$PATH:$PATH/bin
```
and add it to your ~/.bashrc PATH export line

## Inspiration
- ExploitDB search inspired and code examples by: https://github.com/Zenithar/exploitdb
