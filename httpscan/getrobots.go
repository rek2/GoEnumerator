package httpscan

import (
	"crypto/tls"
	"io/ioutil"
	"log"
	"net/http"
)

// Function to check for validity of domain names
func GetRobots(host, port string) string {

	// This is to ignore self made TLS certs
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	// test if for port, this needs to change to check for TLS instead since this may be
	// diff port than 443
	var url string

	if port == "443" {
		url = "https://" + host + ":" + port + "/robots.txt" //strconv.Itoa(port)
	} else {
		url = "http://" + host + ":" + port + "/robots.txt" //strconv.Itoa(port)
	}
	// Fetch the URL
	response, err := http.Get(url)
	if err != nil {
		log.Println("Error fetching URL. ", err)
	}

	defer response.Body.Close()

	// Read the response
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Println("Error reading HTTP body. ", err)
	}

	targetRobots := string(body)
	return targetRobots
}
