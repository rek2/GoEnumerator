// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/httpscan"
	"fmt"

	"github.com/spf13/cobra"
)

// getCommentsCmd represents the getComments command
var getCommentsCmd = &cobra.Command{
	Use:   "getComments",
	Short: "Scrap domain website for comments",
	Long:  `Will scrap the domain site for comments in the html`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("getComments called")
	},
}

var commentsProjectName, commentsHostName string

func init() {
	webToolsCmd.AddCommand(getCommentsCmd)

	getCommentsCmd.Run = getcomments

	getCommentsCmd.Flags().StringVarP(&commentsProjectName, "project", "p", "", "Project name")
	getCommentsCmd.MarkFlagRequired("project")
	getCommentsCmd.Flags().StringVarP(&commentsHostName, "host", "i", "", "Host IP")

}

func getcomments(cmd *cobra.Command, args []string) {

	if len(commentsHostName) > 0 {

		//here we do it with one host, no need to check for list of hosts.
		fmt.Println("NOT YET IMPLEMENTED, SOON!")
	} else {

		allHosts := database.GetHosts(commentsProjectName)
		for host := 0; host < len(allHosts); host++ {
			var allComments []string
			allHTTPports := database.GetHTTPPorts(commentsProjectName, allHosts[host])

			if len(allHTTPports) > 0 {
				allDomains := database.GetDomain(commentsProjectName, allHosts[host])
				for port := 0; port < len(allHTTPports); port++ {
					// if the host has domains do the domains instead of ip, I should change the logic later
					if len(allDomains) > 0 {
						for domain := 0; domain < len(allDomains); domain++ {
							comments := httpscan.GetComments(allDomains[domain], allHTTPports[port])
							if len(comments) > 0 {
								for _, comment := range comments {
									allComments = append(allComments, allDomains[domain]+","+allHTTPports[port]+","+comment)
								}
							}
						}
					} else {
						comments := httpscan.GetComments(allHosts[host], allHTTPports[port])

						if len(comments) > 0 {
							for _, comment := range comments {
								allComments = append(allComments, allHosts[host]+","+allHTTPports[port]+","+comment)
							}
						}
					}
				}
				if len(allComments) > 0 {
					fmt.Printf("\nComments for Host: %s\n", allHosts[host])
					fmt.Println(allComments)
					database.AddComments(commentsProjectName, allHosts[host], allComments)
				}
			} else {
				fmt.Printf("\nNo HTTP ports in the database for HOST: %s, Run a portscan first!!!!\n", allHosts[host])
			}
		}

	}
}
