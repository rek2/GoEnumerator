// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/httpscan"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
)

// getRobotCmd represents the getRobot command
var getRobotCmd = &cobra.Command{
	Use:   "getRobot",
	Short: "Get Robot.txt if exists",
	Long:  `Grabs a host or all hosts from a project and scans for robots.txt files`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("getRobot called")
	},
}

var robotProjectName, RobotHostName string

func init() {
	webToolsCmd.AddCommand(getRobotCmd)

	getRobotCmd.Run = getrobot

	getRobotCmd.Flags().StringVarP(&robotProjectName, "project", "p", "", "Project name")
	getRobotCmd.MarkFlagRequired("project")
	getRobotCmd.Flags().StringVarP(&RobotHostName, "host", "i", "", "Host IP")

}

func getrobot(cmd *cobra.Command, args []string) {

	if len(RobotHostName) > 0 {

		//here we do it with one host, no need to check for list of hosts.
		fmt.Println("NOT YET IMPLEMENTED, SOON!")
	} else {

		allHosts := database.GetHosts(robotProjectName)
		for host := 0; host < len(allHosts); host++ {
			var allRobots []string
			allHTTPports := database.GetHTTPPorts(robotProjectName, allHosts[host])

			if len(allHTTPports) > 0 {
				allDomains := database.GetDomain(robotProjectName, allHosts[host])
				for port := 0; port < len(allHTTPports); port++ {
					// if the host has domains do the domains instead of ip, I should change the logic later
					if len(allDomains) > 0 {
						for domain := 0; domain < len(allDomains); domain++ {
							robots := httpscan.GetRobots(allDomains[domain], allHTTPports[port])
							if len(robots) > 0 {
								if !strings.Contains(robots, "Not Found") || !strings.Contains(robots, "404") {
									allRobots = append(allRobots, allDomains[domain]+","+allHTTPports[port]+","+robots)
								}
							}
						}
					} else {
						robots := httpscan.GetRobots(allHosts[host], allHTTPports[port])

						if len(robots) > 0 {
							if !strings.Contains(robots, "Not Found") || !strings.Contains(robots, "404") {
								allRobots = append(allRobots, allHosts[host]+","+allHTTPports[port]+","+robots)
							}
						}
					}
				}
				if len(allRobots) > 0 {
					fmt.Printf("\nRobot.txt for Host: %s\n", allHosts[host])
					fmt.Println(allRobots)
					database.AddRobots(robotProjectName, allHosts[host], allRobots)
				}
			} else {
				fmt.Printf("\nNo HTTP ports in the database for HOST: %s, Run a portscan first!!!!\n", allHosts[host])
			}
		}

	}
}
