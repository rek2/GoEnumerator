module gitlab.com/rek2/GoEnumerator

go 1.12

require (
	github.com/RoaringBitmap/roaring v0.4.18 // indirect
	github.com/Smerity/govarint v0.0.0-20150407073650-7265e41f48f1 // indirect
	github.com/blevesearch/bleve v0.7.0
	github.com/blevesearch/go-porterstemmer v1.0.2 // indirect
	github.com/blevesearch/segment v0.0.0-20160915185041-762005e7a34f // indirect
	github.com/boltdb/bolt v1.3.1 // indirect
	github.com/couchbase/vellum v0.0.0-20190626091642-41f2deade2cf // indirect
	github.com/edsrzf/mmap-go v1.0.0 // indirect
	github.com/lib/pq v1.1.1
	github.com/mitchellh/go-homedir v1.1.0
	github.com/spf13/cobra v0.0.5
	github.com/spf13/viper v1.4.0
	github.com/steveyen/gtreap v0.0.0-20150807155958-0abe01ef9be2 // indirect
	golang.org/x/sync v0.0.0-20181221193216-37e7f081c4d4
)
