// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/portscan"
	"fmt"

	"github.com/spf13/cobra"
)

// getBannersCmd represents the getBanners command
var getBannersCmd = &cobra.Command{
	Use:   "getBanners",
	Short: "gets ports found on database and try's to get the banner of what is running",
	Long:  `Uses the ports already in the database and creates a socket connection to try to get a banner back.`,
}

var BannersProjectName, BannersHostName string

func init() {
	portscanCmd.AddCommand(getBannersCmd)

	getBannersCmd.Run = getbanners

	getBannersCmd.Flags().StringVarP(&BannersProjectName, "project", "p", "", "Project name")
	getBannersCmd.MarkFlagRequired("project")
	getBannersCmd.Flags().StringVarP(&BannersHostName, "host", "i", "", "Host IP")

}

func getbanners(cmd *cobra.Command, args []string) {

	if len(BannersHostName) > 0 {
		allHosts := database.GetHosts(BannersProjectName)
		fmt.Println(allHosts)
		if Contains(allHosts, BannersHostName) == true {
			fmt.Printf("Doing host: %s\n", BannersHostName)
			ports := database.GetPorts(BannersProjectName, BannersHostName)
			//ports := strings.Split(allPorts, " ")
			for _, port := range ports {
				fmt.Printf("doing port: %s\n", port)
				banner := portscan.GrabBanner(BannersHostName, port)
				if banner != "0" {
					fmt.Printf("- Port: %s\n- Banner: %s\n", port, banner)
				}
			}
			// add banner to database here
		}
	} else {

		allHosts := database.GetHosts(BannersProjectName)
		for host := 0; host < len(allHosts); host++ {
			fmt.Printf("Doing host: %s\n", allHosts[host])
			ports := database.GetPorts(BannersProjectName, allHosts[host])
			var banners []string
			var httpPorts []string
			for _, port := range ports {
				fmt.Printf("doing port: %s\n", port)
				banner := portscan.GrabBanner(allHosts[host], port)
				if banner != "0" {
					fmt.Printf("- Port: %s\n- Banner: %s\n", port, banner)
					banners = append(banners, port+","+banner)
				} else {
					web := portscan.IsHTTP(allHosts[host], port)
					if web == true {
						banner := portscan.GetHttpServer(allHosts[host], port)
						fmt.Printf("- Port: %s\n- Banner: %s\n", port, banner)
						banners = append(banners, port+","+banner)
						httpPorts = append(httpPorts, port)
					}

				}
			}
			database.AddBanners(BannersProjectName, allHosts[host], banners)
			database.AddHTTPPorts(BannersProjectName, allHosts[host], httpPorts)
		}

	}

}
