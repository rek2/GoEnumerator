package httpscan

import (
	"crypto/tls"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
)

func GetJavaScript(host, port string) []string {

	// This is to ignore self made TLS certs
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: true}

	// test if for port, this needs to change to check for TLS instead since this may be
	// diff port than 443
	var url string

	if port == "443" {
		url = "https://" + host + ":" + port //strconv.Itoa(port)
	} else {
		url = "http://" + host + ":" + port //strconv.Itoa(port)
	}

	// Fetch the URL and get response
	response, err := http.Get(url)
	if err != nil {
		log.Fatal("Error fetching URL. ", err)
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatal("Error reading HTTP body. ", err)
	}

	// Look for HTML comments using a regular expression
	re := regexp.MustCompile("<script>(.|\n)*?</script>")
	matches := re.FindAllString(string(body), -1)

	return matches
}
