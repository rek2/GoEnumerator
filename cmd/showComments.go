// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showCommentsCmd represents the showComments command
var showCommentsCmd = &cobra.Command{
	Use:   "showComments",
	Short: "Show comments scrape from website",
	Long:  `Show database entries that have comments scraped from a website for a given project`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("showComments called")
	},
}

var showCommentsProjectName string

func init() {
	webToolsCmd.AddCommand(showCommentsCmd)

	showCommentsCmd.Run = showcomments

	showCommentsCmd.Flags().StringVarP(&showCommentsProjectName, "project", "p", "", "Project name")
	showCommentsCmd.MarkFlagRequired("project")
	//getRobotCmd.Flags().StringVarP(&RobotHostName, "host", "i", "", "Host IP")

}

func showcomments(cmd *cobra.Command, args []string) {

	hosts := database.GetHosts(showCommentsProjectName)
	for _, host := range hosts {
		comments := database.GetComments(showCommentsProjectName, host)
		fmt.Printf("comments found for Host: %s\n", host)
		for comment := range comments {
			fmt.Printf("-  %s\n", comments[comment])
		}
	}
}
