// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showJavaScriptCmd represents the showJavaScript command
var showJavaScriptCmd = &cobra.Command{
	Use:   "showJavaScript",
	Short: "Show captured javascript from domains",
	Long:  `Show captured javascript from the project domains`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("showJavaScript called")
	},
}

var showJavaScriptProjectName string

func init() {
	webToolsCmd.AddCommand(showJavaScriptCmd)

	showJavaScriptCmd.Run = showjavascript

	showJavaScriptCmd.Flags().StringVarP(&showJavaScriptProjectName, "project", "p", "", "Project name")
	showJavaScriptCmd.MarkFlagRequired("project")
	//getRobotCmd.Flags().StringVarP(&RobotHostName, "host", "i", "", "Host IP")

}

func showjavascript(cmd *cobra.Command, args []string) {

	hosts := database.GetHosts(showJavaScriptProjectName)
	for _, host := range hosts {
		javaScript := database.GetJavaScript(showJavaScriptProjectName, host)
		fmt.Printf("JavaScript found for Host: %s\n", host)
		for java := range javaScript {
			fmt.Printf("-  %s\n", javaScript[java])
		}
	}
}
