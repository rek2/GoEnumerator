// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showRobotCmd represents the showRobot command
var showRobotsCmd = &cobra.Command{
	Use:   "showRobots",
	Short: "Show robots.txt data save in the database",
	Long:  `Show the robots.txt info we have saved in the database for certain project`,
	//Run: func(cmd *cobra.Command, args []string) {
	//	fmt.Println("showRobot called")
	//},
}

var showRobotsProjectName string

func init() {
	webToolsCmd.AddCommand(showRobotsCmd)

	showRobotsCmd.Run = showrobots

	showRobotsCmd.Flags().StringVarP(&showRobotsProjectName, "project", "p", "", "Project name")
	showRobotsCmd.MarkFlagRequired("project")
	//getRobotCmd.Flags().StringVarP(&RobotHostName, "host", "i", "", "Host IP")

}

func showrobots(cmd *cobra.Command, args []string) {

	hosts := database.GetHosts(showRobotsProjectName)
	for _, host := range hosts {
		robots := database.GetRobots(showRobotsProjectName, host)
		fmt.Printf("robots.txt for Host: %s\n", host)
		for robot := range robots {
			fmt.Printf("-  %s\n", robots[robot])
		}
	}
}
