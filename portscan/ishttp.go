package portscan

import (
	"net/http"
)

func IsHTTP(Host string, Port string) bool {

	url := "http://" + Host + ":" + Port //strconv.Itoa(port)

	_, err := http.Get(url)

	if err == nil {
		//fmt.Println("+ Found a webserver on: ", Port)
		return true
	}
	return false
}
