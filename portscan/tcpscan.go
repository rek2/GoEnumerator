package portscan

import (
	"context"
	"fmt"
	"net"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"golang.org/x/sync/semaphore"
)

type PortScanner struct {
	ip   string
	lock *semaphore.Weighted
}

// This was picket from an example online and modified, but I do not like it.
// will change soon to no need ulimit so works on non-unix
func Ulimit() int64 {
	out, err := exec.Command("sh", "-c", "ulimit -n").Output()

	if err != nil {
		panic(err)
	}

	s := strings.TrimSpace(string(out))
	if s == "unlimited" {
		s = "4096"
	}

	//fmt.Println(s)

	i, err := strconv.ParseInt(s, 10, 64)
	if err != nil {
		panic(err)
	}

	i = i / 10
	return i
}

func ScanPort(ip string, port int, timeout time.Duration) int {
	target := fmt.Sprintf("%s:%d", ip, port)
	conn, err := net.DialTimeout("tcp", target, timeout)

	if err != nil {
		if strings.Contains(err.Error(), "too many open files") {
			time.Sleep(timeout)
			ScanPort(ip, port, timeout)
		}

		return 0
	}

	conn.Close()
	fmt.Println(port, "open")
	return port
}

func (ps *PortScanner) Start(f, l int, timeout time.Duration) []int {
	var openPorts []int
	wg := sync.WaitGroup{}
	defer wg.Wait()

	for port := f; port <= l; port++ {
		ps.lock.Acquire(context.TODO(), 1)
		wg.Add(1)
		go func(port int) {
			defer ps.lock.Release(1)
			defer wg.Done()
			openport := ScanPort(ps.ip, port, timeout)
			if openport != 0 {
				openPorts = append(openPorts, openport)
			}
		}(port)
	}
	return openPorts
}

func TcpScan(ipToScan string, minPort int, maxPort int) []int {
	ps := &PortScanner{
		ip:   ipToScan,
		lock: semaphore.NewWeighted(Ulimit()),
	}
	fmt.Printf("\nScanning: %s\n\n", ipToScan)
	openPorts := ps.Start(minPort, maxPort, 5000*time.Millisecond)
	return openPorts
}
