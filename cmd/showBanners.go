// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showBannersCmd represents the showBanners command
var showBannersCmd = &cobra.Command{
	Use:   "showBanners",
	Short: "Show saved banners for each host",
	Long:  `Show saved banners in the database for each host in a project`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("showBanners called")
	},
}

func init() {
	portscanCmd.AddCommand(showBannersCmd)

	showBannersCmd.Run = showbanners

	showBannersCmd.Flags().StringVarP(&ShowPortsProjectName, "project", "p", "", "Project name")
	showBannersCmd.MarkFlagRequired("project")

}

func showbanners(cmd *cobra.Command, args []string) {
	hosts := database.GetHosts(ShowPortsProjectName)
	for _, host := range hosts {
		banners := database.GetBanners(ShowPortsProjectName, host)
		fmt.Printf("Banners for Host: %s\n", host)
		for banner := range banners {
			fmt.Printf("-  %s\n", banners[banner])
		}
	}

}
