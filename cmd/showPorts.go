// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showPortsCmd represents the showPorts command
var showPortsCmd = &cobra.Command{
	Use:   "showPorts",
	Short: "Show ports",
	Long:  `Show ports saved in the database.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("showPorts called")
	},
}

var ShowPortsProjectName string

func init() {
	portscanCmd.AddCommand(showPortsCmd)

	showPortsCmd.Run = showports

	showPortsCmd.Flags().StringVarP(&ShowPortsProjectName, "project", "p", "", "Project name")
	showPortsCmd.MarkFlagRequired("project")

}

func showports(cmd *cobra.Command, args []string) {
	hosts := database.GetHosts(ShowPortsProjectName)
	for _, host := range hosts {
		ports := database.GetPorts(ShowPortsProjectName, host)
		fmt.Printf("Ports open on Host: %s\n", host)
		for port := range ports {
			fmt.Printf("-  %s\n", ports[port])
		}
	}

}
