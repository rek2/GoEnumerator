// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/httpscan"
	"fmt"

	"github.com/spf13/cobra"
)

// getJavaScriptCmd represents the getJavaScript command
var getJavaScriptCmd = &cobra.Command{
	Use:   "getJavaScript",
	Short: "Scrap domain website for javascript",
	Long:  `Will scrap the domain site for javascript in the html page`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("getJavaScript called")
	},
}

var javaScriptProjectName, javaScriptHostName string

func init() {
	webToolsCmd.AddCommand(getJavaScriptCmd)

	getJavaScriptCmd.Run = getjavascript

	getJavaScriptCmd.Flags().StringVarP(&javaScriptProjectName, "project", "p", "", "Project name")
	getJavaScriptCmd.MarkFlagRequired("project")
	getJavaScriptCmd.Flags().StringVarP(&javaScriptHostName, "host", "i", "", "Host IP")

}

func getjavascript(cmd *cobra.Command, args []string) {

	if len(commentsHostName) > 0 {

		//here we do it with one host, no need to check for list of hosts.
		fmt.Println("NOT YET IMPLEMENTED, SOON!")
	} else {

		allHosts := database.GetHosts(javaScriptProjectName)
		for host := 0; host < len(allHosts); host++ {
			var allJavaScript []string
			allHTTPports := database.GetHTTPPorts(javaScriptProjectName, allHosts[host])

			if len(allHTTPports) > 0 {
				allDomains := database.GetDomain(javaScriptProjectName, allHosts[host])
				for port := 0; port < len(allHTTPports); port++ {
					// if the host has domains do the domains instead of ip, I should change the logic later
					if len(allDomains) > 0 {
						for domain := 0; domain < len(allDomains); domain++ {
							javaScripts := httpscan.GetJavaScript(allDomains[domain], allHTTPports[port])
							if len(javaScripts) > 0 {
								for _, java := range javaScripts {
									allJavaScript = append(allJavaScript, allDomains[domain]+","+allHTTPports[port]+","+java)
								}
							}
						}
					} else {
						javaScripts := httpscan.GetJavaScript(allHosts[host], allHTTPports[port])

						if len(javaScripts) > 0 {
							for _, java := range javaScripts {
								allJavaScript = append(allJavaScript, allHosts[host]+","+allHTTPports[port]+","+java)
							}
						}
					}
				}
				if len(allJavaScript) > 0 {
					fmt.Printf("\nAll JavaScript for Host: %s\n", allHosts[host])
					fmt.Println(allJavaScript)
					database.AddJavaScript(javaScriptProjectName, allHosts[host], allJavaScript)
				}
			} else {
				fmt.Printf("\nNo HTTP ports in the database for HOST: %s, Run a portscan first!!!!\n", allHosts[host])
			}
		}

	}
}
