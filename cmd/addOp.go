// Copyright © 2018 Rek2, Fernandez rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"

	"github.com/spf13/cobra"
)

// addOpCmd represents the addOp command
var addOpCmd = &cobra.Command{
	Use:   "addOp",
	Short: "Add new operations/project name to database",
	Args:  cobra.ExactArgs(1),
	Long:  `This is the first command you will run to start your project or operation`,
	Run: func(cmd *cobra.Command, args []string) {
		database.CreateProject(args)
	},
}

func init() {
	projectCmd.AddCommand(addOpCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// addOpCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// addOpCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
