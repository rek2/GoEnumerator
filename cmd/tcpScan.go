// Copyright © 2018 Rek2, Fernandez rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/portscan"
	"fmt"
	"strconv"

	"github.com/spf13/cobra"
)

// tcpScanCmd represents the tcpScan command
var tcpScanCmd = &cobra.Command{
	Use:   "tcpScan",
	Short: "Simple tcp port scan",
	Long:  `a simple TCP stule full handshake port scan`,
}

var ScanProjectName, ScanHostName string

var MinPort, MaxPort int

func init() {
	portscanCmd.AddCommand(tcpScanCmd)

	tcpScanCmd.Run = tcpScan

	tcpScanCmd.Flags().StringVarP(&ScanProjectName, "project", "p", "", "Project name")
	tcpScanCmd.MarkFlagRequired("project")
	tcpScanCmd.Flags().StringVarP(&ScanHostName, "host", "i", "", "Host IP")
	tcpScanCmd.Flags().IntVarP(&MinPort, "startport", "s", 1, "Starting Port")
	tcpScanCmd.Flags().IntVarP(&MaxPort, "endport", "e", 1024, "Ending Port")
}

func tcpScan(cmd *cobra.Command, args []string) {

	if len(ScanHostName) > 0 {
		allHosts := database.GetHosts(ScanProjectName)
		if Contains(allHosts, ScanHostName) == true {
			openPorts := portscan.TcpScan(ScanHostName, MinPort, MaxPort)
			fmt.Println(openPorts)
			// add to Database #todo
		}

	} else {
		allHosts := database.GetHosts(ScanProjectName)

		for host := 0; host < len(allHosts); host++ {
			var openPortsString []string
			openPorts := portscan.TcpScan(allHosts[host], MinPort, MaxPort)
			//allPorts := database.GetPorts(ScanProjectName, ScanHostName)
			for _, port := range openPorts {
				openPortsString = append(openPortsString, strconv.Itoa(port))
			}
			// ports := strings.Join(openPortsString, " ")
			ports := openPortsString
			database.AddPorts(ScanProjectName, allHosts[host], ports)
		}
	}

}

// Contains tells whether the host slice contains given host.
func Contains(allHosts []string, host string) bool {
	for _, n := range allHosts {
		if host == n {
			return true
		}
	}
	return false

}

func ContainsInt(allPorts []int, port int) bool {
	for _, n := range allPorts {
		if port == n {
			return true
		}
	}
	return false
}
