// Copyright © 2018 ReK2, Fernandez rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package database

import (
	"database/sql"
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/lib/pq"
	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

// read config
func GetConfig() (string, string, string, string) {
	// Find home directory.
	home, err := homedir.Dir()
	if err != nil {
		fmt.Println(err)
		os.Exit(1)

		// Search config in home directory with name ".GoEnumerator" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".GoEnumerator")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		//	fmt.Println("Using config file:", viper.ConfigFileUsed())
	}

	db_host := viper.GetString("database.dbhost")
	db_user := viper.GetString("database.dbuser")
	db_pass := viper.GetString("database.dbpassword")
	db_name := viper.GetString("database.dbname")

	return db_user, db_pass, db_name, db_host
}

// DbConnect to be rehused
func DbConnect() *sql.DB {

	db_user, db_password, db_name, db_host := GetConfig()
	dbinfo := fmt.Sprintf("user=%s password=%s dbname=%s host=%s sslmode=disable",
		db_user, db_password, db_name, db_host)
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		fmt.Println(err)
	}

	return db

}

func CreateProject(ProjectName []string) {

	name := strings.Join(ProjectName, "")

	fmt.Println(name)

	db := DbConnect()
	_, err := db.Exec(`CREATE TABLE ` + name + `(
    ID SERIAL PRIMARY KEY NOT NULL,
    HOST    TEXT    NOT NULL,
	HOSTNAME TEXT[],
    PORTS   TEXT[],
    BANNERS TEXT[],
	HTTPPORTS TEXT[],
    JAVASCRIPT TEXT[],
    HEADERS TEXT[],
    COMMENTS TEXT[],
    ROBOTS  TEXT[],
    EMAILS  TEXT[],
    CMS     TEXT[],
    URLS    TEXT[],
    BUSTIN  TEXT[],
    VULN    TEXT[]
    )`)

	if err != nil {
		panic(err)
	}
	db.Close()
}

func DropProject(ProjectName []string) {

	name := strings.Join(ProjectName, "")
	db := DbConnect()

	_, err := db.Exec(`DROP TABLE IF EXISTS ` + name)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func AddHost(ProjectName string, HostName string) {
	fmt.Println(ProjectName, HostName)
	db := DbConnect()

	_, err := db.Exec(`INSERT INTO `+ProjectName+`(HOST) VALUES($1) RETURNING ID`, HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func RemoveHost(ProjectName string, HostName string) {
	fmt.Println(ProjectName, HostName)
	db := DbConnect()

	_, err := db.Exec(`DELETE FROM `+ProjectName+` WHERE HOST=$1 RETURNING ID`, HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func ShowOps() {

	db := DbConnect()

	rows, err := db.Query(`SELECT tablename FROM pg_catalog.pg_tables WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'`)

	if err != nil {
		panic(err)
	}
	defer rows.Close()
	for rows.Next() {
		var operation string
		if err := rows.Scan(&operation); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("Operation: %s \n", operation)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
	db.Close()
}

func ShowHosts(ProjectName string) {

	db := DbConnect()

	rows, err := db.Query(`SELECT host from ` + ProjectName)

	if err != nil {
		panic(err)
	}
	defer rows.Close()
	fmt.Println("--- Operation " + ProjectName + " ---")
	fmt.Println("Hosts:")
	for rows.Next() {
		var host string
		if err := rows.Scan(&host); err != nil {
			log.Fatal(err)
		}
		fmt.Printf("- %s \n", host)
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	db.Close()

}

func GetHosts(ProjectName string) []string {

	db := DbConnect()
	var allHosts []string

	rows, err := db.Query(`SELECT host from ` + ProjectName)

	if err != nil {
		panic(err)
	}
	defer rows.Close()

	for rows.Next() {
		var host string
		if err := rows.Scan(&host); err != nil {
			log.Fatal(err)
		}
		allHosts = append(allHosts, host)
	}

	db.Close()

	return allHosts
}

func AddPorts(ProjectName string, HostName string, openPorts []string) {

	db := DbConnect()

	_, err := db.Exec(`UPDATE `+ProjectName+` SET PORTS=$1 WHERE HOST=$2`, pq.Array(openPorts), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func GetPorts(ProjectName string, Host string) (ports []string) {

	db := DbConnect()

	query := "SELECT ports FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&ports)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddBanners(ProjectName string, HostName string, Banner []string) {

	db := DbConnect()
	_, err := db.Exec(`UPDATE `+ProjectName+` SET BANNERS=$1 WHERE HOST=$2`, pq.Array(Banner), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func GetBanners(ProjectName string, Host string) (banners []string) {

	db := DbConnect()

	query := "SELECT banners FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&banners)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddHTTPPorts(ProjectName string, HostName string, httpPorts []string) {

	db := DbConnect()

	_, err := db.Exec(`UPDATE `+ProjectName+` SET HTTPPORTS=$1 WHERE HOST=$2`, pq.Array(httpPorts), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func GetHTTPPorts(ProjectName string, Host string) (ports []string) {

	db := DbConnect()

	query := "SELECT httpports FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&ports)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddDomain(ProjectName string, IP string, HostName []string) {

	db := DbConnect()
	var currentNames []string
	currentNames = GetDomain(ProjectName, IP)

	if currentNames != nil {
		// Here we should check if hostname is already in current slice.
		HostName = append(HostName, currentNames...)

	}
	_, err := db.Exec(`UPDATE `+ProjectName+` SET HOSTNAME=$1 WHERE HOST=$2`, pq.Array(HostName), IP)
	if err != nil {
		panic(err)
	}
	db.Close()

}

func RemoveDomain(ProjectName string, IP string, Domain string) bool {

	db := DbConnect()
	var currentNames []string
	currentNames = GetDomain(ProjectName, IP)

	if currentNames != nil {
		for _, name := range currentNames {
			if name == Domain {
				Domains := removeString(currentNames, Domain)
				_, err := db.Exec(`UPDATE `+ProjectName+` SET HOSTNAME=$1 WHERE HOST=$2`, pq.Array(Domains), IP)
				if err != nil {
					panic(err)
				}
				return true
			}
		}
	}
	db.Close()
	return false
}

func GetDomain(ProjectName string, Host string) (hostnames []string) {

	db := DbConnect()

	query := "SELECT hostname FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&hostnames)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddRobots(ProjectName string, HostName string, Robots []string) {

	db := DbConnect()
	_, err := db.Exec(`UPDATE `+ProjectName+` SET ROBOTS=$1 WHERE HOST=$2`, pq.Array(Robots), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func GetRobots(ProjectName string, Host string) (robots []string) {

	db := DbConnect()

	query := "SELECT robots FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&robots)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddComments(ProjectName string, HostName string, Comments []string) {

	db := DbConnect()
	_, err := db.Exec(`UPDATE `+ProjectName+` SET COMMENTS=$1 WHERE HOST=$2`, pq.Array(Comments), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func GetComments(ProjectName string, Host string) (comments []string) {

	db := DbConnect()

	query := "SELECT comments FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&comments)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddEmails(ProjectName string, HostName string, Emails []string) {

	db := DbConnect()
	_, err := db.Exec(`UPDATE `+ProjectName+` SET EMAILS=$1 WHERE HOST=$2`, pq.Array(Emails), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()
}

func GetEmails(ProjectName string, Host string) (emails []string) {

	db := DbConnect()

	query := "SELECT emails FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&emails)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddJavaScript(ProjectName string, HostName string, JavaScript []string) {

	db := DbConnect()
	_, err := db.Exec(`UPDATE `+ProjectName+` SET JAVASCRIPT=$1 WHERE HOST=$2`, pq.Array(JavaScript), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()
}

func GetJavaScript(ProjectName string, Host string) (javascripts []string) {

	db := DbConnect()

	query := "SELECT javascript FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&javascripts)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddHeaders(ProjectName string, HostName string, Headers []string) {

	db := DbConnect()
	_, err := db.Exec(`UPDATE `+ProjectName+` SET HEADERS=$1 WHERE HOST=$2`, pq.Array(Headers), HostName)

	if err != nil {
		panic(err)
	}
	db.Close()

}

func GetHeaders(ProjectName string, Host string) (headers []string) {

	db := DbConnect()

	query := "SELECT headers FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&headers)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func AddExploitDbId(ProjectName string, IP string, ID string) {

	db := DbConnect()
	var currentVulns, url []string
	currentVulns = GetExploitDbId(ProjectName, IP)

	url = append(url, "https://www.exploit-db.com/exploits/"+ID)

	if currentVulns != nil {
		// Here we should check if vuln is already in current slice.
		url = append(url, currentVulns...)

	}

	_, err := db.Exec(`UPDATE `+ProjectName+` SET VULN=$1 WHERE HOST=$2`, pq.Array(url), IP)
	if err != nil {
		panic(err)
	}

	db.Close()

}

func RemoveExploitDbId(ProjectName string, IP string, ID string) bool {

	db := DbConnect()
	var currentVulns []string
	currentVulns = GetExploitDbId(ProjectName, IP)

	url := "https://www.exploit-db.com/exploits/" + ID

	if currentVulns != nil {
		for _, entrie := range currentVulns {
			if entrie == url {
				urls := removeString(currentVulns, url)
				_, err := db.Exec(`UPDATE `+ProjectName+` SET VULN=$1 WHERE HOST=$2`, pq.Array(urls), IP)
				if err != nil {
					panic(err)
				}
				return true
			}
		}
	}
	db.Close()
	return false
}

func GetExploitDbId(ProjectName string, Host string) (vuln []string) {

	db := DbConnect()

	query := "SELECT vuln FROM " + ProjectName + " WHERE host=$1"

	if err := db.QueryRow(query, Host).Scan(pq.Array(&vuln)); err != nil {
		log.Fatal(err)
	}

	db.Close()

	return
}

func removeString(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

//func isValueInList(value []string, list []string) bool {
//	for _, v := range list {
//		if v == value {
//			return true
//		}
//	}
//	return false
//}
