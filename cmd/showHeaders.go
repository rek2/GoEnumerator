// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showHeadersCmd represents the showHeaders command
var showHeadersCmd = &cobra.Command{
	Use:   "showHeaders",
	Short: "Show the domain headers on the database",
	Long:  `Show the domain headers on the database for each host and domain`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("showHeaders called")
	},
}

var showHeadersProjectName string

func init() {
	webToolsCmd.AddCommand(showHeadersCmd)

	showHeadersCmd.Run = showheaders

	showHeadersCmd.Flags().StringVarP(&showHeadersProjectName, "project", "p", "", "Project name")
	showHeadersCmd.MarkFlagRequired("project")
	//getRobotCmd.Flags().StringVarP(&RobotHostName, "host", "i", "", "Host IP")

}

func showheaders(cmd *cobra.Command, args []string) {

	hosts := database.GetHosts(showHeadersProjectName)
	for _, host := range hosts {
		headers := database.GetHeaders(showHeadersProjectName, host)
		fmt.Printf("Headers found for Host: %s\n", host)
		for header := range headers {
			fmt.Printf("-  %s\n", headers[header])
		}
	}
}
