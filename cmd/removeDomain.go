// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/httpscan"
	"fmt"

	"github.com/spf13/cobra"
)

// removeDomainCmd represents the removeDomain command
var removeDomainCmd = &cobra.Command{
	Use:   "removeDomain",
	Short: "Remove a domain from the list of domain names",
	Long:  `Remove a domain from the list of domain names for a host(ip)`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("removeDomain called")
	},
}

var RemoveDomain, DomainProject, DomainIP string

func init() {
	projectCmd.AddCommand(removeDomainCmd)

	removeDomainCmd.Run = removedomain

	removeDomainCmd.Flags().StringVarP(&DomainProject, "project", "p", "", "Project name")
	removeDomainCmd.Flags().StringVarP(&DomainIP, "host", "i", "", "Host IP")
	removeDomainCmd.Flags().StringVarP(&RemoveDomain, "domain", "d", "", "Domain Name or URL")
	removeDomainCmd.MarkFlagRequired("project")
	removeDomainCmd.MarkFlagRequired("host")
	removeDomainCmd.MarkFlagRequired("domain")

}

func removedomain(cmd *cobra.Command, args []string) {

	name := httpscan.CheckDomain(RemoveDomain)
	if name != nil {
		fmt.Println("Please enter a valid domain name, NOT an IP for an IP add it to removeHost")
	} else {
		found := database.RemoveDomain(DomainProject, DomainIP, RemoveDomain)
		if found == false {
			fmt.Println("Domain not found!!! check for typos!")
		}
	}
}
