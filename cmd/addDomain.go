// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/httpscan"
	"fmt"

	"github.com/spf13/cobra"
)

// addDomainCmd represents the addDomain command
var addDomainCmd = &cobra.Command{
	Use:   "addDomain",
	Short: "Add hostname or domain to a project IP/host",
	Long: `Add a hostname or a domain name to a current project IP/Host like vhosts tied to one IP.
	example: targethost.com and test.targethome.com`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("addDomain called")
	},
}

var AddDomainName, AddDomainProject, AddDomainIP string

func init() {
	projectCmd.AddCommand(addDomainCmd)

	addDomainCmd.Run = adddomain

	addDomainCmd.Flags().StringVarP(&AddDomainProject, "project", "p", "", "Project name")
	addDomainCmd.Flags().StringVarP(&AddDomainIP, "host", "i", "", "Host IP")
	addDomainCmd.Flags().StringVarP(&AddDomainName, "domain", "d", "", "Domain Name or URL")
	addDomainCmd.MarkFlagRequired("project")
	addDomainCmd.MarkFlagRequired("host")
	addDomainCmd.MarkFlagRequired("domain")

}

func adddomain(cmd *cobra.Command, args []string) {

	name := httpscan.CheckDomain(AddDomainName)
	if name != nil {
		fmt.Println("Please enter a valid domain name, NOT an IP for an IP add it to addHost")
	} else {
		var domains []string
		domains = append(domains, AddDomainName)
		database.AddDomain(AddDomainProject, AddDomainIP, domains)
	}
}
