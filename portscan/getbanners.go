package portscan

import (
	"net"
	"time"
)

func GrabBanner(ip string, port string) string {
	connection, err := net.DialTimeout(
		"tcp",
		ip+":"+port,
		time.Second*10)
	if err != nil {
		return "0"
	}

	// See if server offers anything to read
	buffer := make([]byte, 4096)
	connection.SetReadDeadline(time.Now().Add(time.Second * 5))
	// Set timeout
	numBytesRead, err := connection.Read(buffer)
	if err != nil {
		return "0"
	}

	banner := string(buffer[0:numBytesRead])
	return banner

}
