// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"fmt"

	"github.com/spf13/cobra"
)

// showEmailsCmd represents the showEmails command
var showEmailsCmd = &cobra.Command{
	Use:   "showEmails",
	Short: "Show emails captured in the database",
	Long:  `Show the emails captured to the database`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("showEmails called")
	},
}

var showEmailsProjectName string

func init() {
	webToolsCmd.AddCommand(showEmailsCmd)

	showEmailsCmd.Run = showemails

	showEmailsCmd.Flags().StringVarP(&showEmailsProjectName, "project", "p", "", "Project name")
	showEmailsCmd.MarkFlagRequired("project")
	//getRobotCmd.Flags().StringVarP(&RobotHostName, "host", "i", "", "Host IP")

}

func showemails(cmd *cobra.Command, args []string) {

	hosts := database.GetHosts(showEmailsProjectName)
	for _, host := range hosts {
		emails := database.GetEmails(showEmailsProjectName, host)
		fmt.Printf("Emails found for Host: %s\n", host)
		for email := range emails {
			fmt.Printf("-  %s\n", emails[email])
		}
	}
}
