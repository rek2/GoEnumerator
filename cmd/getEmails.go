// Copyright © 2018 Fernandez, rek2 rek2@hispagatos.org
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

package cmd

import (
	"gitlab.com/rek2/GoEnumerator/database"
	"gitlab.com/rek2/GoEnumerator/httpscan"
	"fmt"

	"github.com/spf13/cobra"
)

// getEmailsCmd represents the getEmails command
var getEmailsCmd = &cobra.Command{
	Use:   "getEmails",
	Short: "Search website for emails",
	Long:  `Search given project or host domains for emails`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("getEmails called")
	},
}

var emailsProjectName, emailsHostName string

func init() {
	webToolsCmd.AddCommand(getEmailsCmd)

	getEmailsCmd.Run = getemails

	getEmailsCmd.Flags().StringVarP(&emailsProjectName, "project", "p", "", "Project name")
	getEmailsCmd.MarkFlagRequired("project")
	getEmailsCmd.Flags().StringVarP(&emailsHostName, "host", "i", "", "Host IP")

}

func getemails(cmd *cobra.Command, args []string) {

	if len(emailsHostName) > 0 {

		//here we do it with one host, no need to check for list of hosts.
		fmt.Println("NOT YET IMPLEMENTED, SOON!")
	} else {

		allHosts := database.GetHosts(emailsProjectName)
		for host := 0; host < len(allHosts); host++ {
			var allEmails []string
			allHTTPports := database.GetHTTPPorts(emailsProjectName, allHosts[host])

			if len(allHTTPports) > 0 {
				allDomains := database.GetDomain(emailsProjectName, allHosts[host])
				for port := 0; port < len(allHTTPports); port++ {
					// if the host has domains do the domains instead of ip, I should change the logic later
					if len(allDomains) > 0 {
						for domain := 0; domain < len(allDomains); domain++ {
							emails := httpscan.GetEmails(allDomains[domain], allHTTPports[port])
							if len(emails) > 0 {
								for _, email := range emails {
									email = email[8 : len(email)-1]
									allEmails = append(allEmails, allDomains[domain]+","+allHTTPports[port]+","+email)
								}
							}
						}
					} else {
						emails := httpscan.GetEmails(allHosts[host], allHTTPports[port])

						if len(emails) > 0 {
							for _, email := range emails {
								email = email[8 : len(email)-1]
								allEmails = append(allEmails, allHosts[host]+","+allHTTPports[port]+","+email)
							}
						}
					}
				}
				if len(allEmails) > 0 {
					fmt.Printf("\nEmails for Host: %s\n", allHosts[host])
					fmt.Println(allEmails)
					database.AddEmails(emailsProjectName, allHosts[host], allEmails)
				}
			} else {
				fmt.Printf("\nNo HTTP ports in the database for HOST: %s, Run a portscan first!!!!\n", allHosts[host])
			}
		}

	}
}
